/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.tees.u0012604;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedHashMap;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import uk.ac.tees.u0012604.examples.Blob1;
import uk.ac.tees.u0012604.examples.Blob2;
import uk.ac.tees.u0012604.examples.Blob3;
import uk.ac.tees.u0012604.examples.BlobFactory;
import uk.ac.tees.u0012604.examples.Blobber;
import uk.ac.tees.u0012604.examples.SynchBlob1;
import uk.ac.tees.u0012604.examples.SynchBlob2;

/**
 *
 * @author steven
 */
final class MainPanel extends JPanel {

    private LinkedHashMap<String, BlobFactory> map = new LinkedHashMap<String, BlobFactory>();

    private String descr
            = // textual description
            "more example programs to demonstrate simple animated graphics...\n"
            + "these use threads with & without different approaches to synchronization  \n\n"
            + "mostly the examples still do not give us the results we want,        \n"
            + "the graphics still mess up & leave traces but can now be interrupted \n"
            + "examine the code & try to explain what is happening & why.           \n\n"
            + "Uncomment the getGraphics call in ThreadBallSubPanel/actionPerformed \n"
            + "- can you explain this? \n\n"
            + "The 'wrapper' uses a factory map. Check this out & form an opinion.    ";

    public MainPanel() {
        // create a global blob
        final Blobber gBlob = new Blob1(Color.BLUE, 20, 20, 2, 0, 25);

        // put tasks in the callerMap
        map.put("example-1",
            new BlobFactory() {
                @Override
                public Blobber makeBlob() {
                    return new Blob1(Color.RED, 20, 20, 2, 3, 25);
                }
            });
        
        map.put("example-2",
            new BlobFactory() {
                @Override
                public Blobber makeBlob() {
                    return gBlob;
                }
            });
        
        map.put("single runner Blob2",
            new BlobFactory() {
                @Override
                public Blobber makeBlob() {
                    return new Blob2(Color.GREEN, 20, 20, 1, -2, 25);
                }
            });
        
        map.put("single runner Blob3",
                new BlobFactory() {
            @Override
            public Blobber makeBlob() {
                return new Blob3(Color.GRAY, 20, 20, 1, -2, 25);
            }
        });

        map.put("synch eg-1",
            new BlobFactory() {
                @Override
                public Blobber makeBlob() {
                    return new SynchBlob1(Color.RED, 20, 20, 2, 3, 25);
                }
            });
        
        map.put("synch eg-2",
            new BlobFactory() {
                @Override
                public Blobber makeBlob() {
                    return new SynchBlob2(Color.RED, 20, 20, 2, 3, 25);
                }
            });

        //--------------------
        // setup GUI
        //--------------------
        // main GUI feature is a JComboBox
        // code is fairly standard - check Java Tutorial etc for additional details
        final JComboBox frameChoice = new JComboBox(map.keySet().toArray());
        add(frameChoice);
        frameChoice.setSelectedIndex(0);		// ie: the default is the first one

        Button runBtn = new Button("run");
        runBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String choice = (String) frameChoice.getSelectedItem();
                new ThreadBallSubPanel(
                    choice,
                    map.get(choice)
                );
            }
        });

        // editor pane for text display
        JEditorPane text = new JEditorPane();
        text.setText(descr);
        text.setEditable(false);

        // GUI layout
        setLayout(new BorderLayout());
        add(text, BorderLayout.CENTER);
        JPanel subPane = new JPanel();
        add(subPane, BorderLayout.SOUTH);
        subPane.add(frameChoice);
        subPane.add(runBtn);
    }
}


final public class AppLauncher extends javax.swing.JFrame {
            
    /**
     * Creates new form AppLauncher
     */
    public AppLauncher() {
        setSize(500, 400);
        add(new MainPanel());
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jTextArea1 = new javax.swing.JTextArea();
        launchBtn = new javax.swing.JButton();
        registeredApps = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 4, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 24, Short.MAX_VALUE)
        );

        jTextArea1.setEditable(false);
        jTextArea1.setColumns(20);
        jTextArea1.setLineWrap(true);
        jTextArea1.setRows(5);
        jTextArea1.setText("Some example programs to demonstrate simple animated graphics...\n(1) using constructors & paint loops & (2) using XOR mode       \n\nMostly these examples do not give us the results we would like,\nthe graphics mess up & there are unwanted delays in opening/closing\nexamine the code & try to explain what is happening & why.\nCorrect it if you can.\n  \nThe AppLauncher 'wrapper' uses some stuff from the Java\nreflections pack. Check this out.\n\nThe original version and inspiration for this launcher application was developed by Simon Lynch.  Parts of it have been rewritten by Steven Mead to include examples of the integration of design patterns, notable Factory and Strategy Patterns.");
        jTextArea1.setWrapStyleWord(true);

        launchBtn.setText("Launch App");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(registeredApps, javax.swing.GroupLayout.PREFERRED_SIZE, 287, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(launchBtn))
                    .addComponent(jTextArea1))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jTextArea1, javax.swing.GroupLayout.PREFERRED_SIZE, 266, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(launchBtn)
                            .addComponent(registeredApps, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AppLauncher.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AppLauncher.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AppLauncher.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AppLauncher.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new AppLauncher().setVisible(true);
            }
        });
        
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JButton launchBtn;
    private javax.swing.JComboBox<String> registeredApps;
    // End of variables declaration//GEN-END:variables
}
