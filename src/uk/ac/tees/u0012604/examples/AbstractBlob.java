package uk.ac.tees.u0012604.examples;

import java.awt.*;
import uk.ac.tees.u0012604.utils.Utils;

public abstract class AbstractBlob implements Blobber {

    private static final int DEFAULT_SIZE = 20;
    private int x, y;				// coordinates
    private int dx, dy;				// motion vector
    private int xlimit, ylimit;		// space boundary (NB: blob stops at limit)
    private int size;
    private int delay;

    private int lifeSpan;			// how long blob will "live"

    private Color color;
    private Graphics g;

    public AbstractBlob(Color color, int x, int y, int dx, int dy, int delay) {
        this.x = x;
        this.y = y;
        this.dx = dx;
        this.dy = dy;

        this.delay = delay;
        this.size = DEFAULT_SIZE;
        this.color = color;
    }

    public abstract void startBlob(Panel canvas, Graphics g, int age);

    public void setCanvas(Panel canvas) {
        xlimit = canvas.getWidth() - size;
        ylimit = canvas.getHeight() - size;
    }

    public Graphics getGraphics() {
        return g;
    }

    public void setGraphics(Graphics g) {
        this.g = g;
    }

    /**
     * snooze for the given delay time
     */
    protected void snooze() {
        Utils.pause(delay);
    }

    /**
     * erase the image of the blob
     */
    protected void erase() {
        g.clearRect(x, y, size, size);
    }

    /**
     * display the image of the blob at its current coordinates
     */
    protected void display() {
        g.setColor(color);
        g.fillOval(x, y, size, size);
    }

    /**
     * move blob by its motion vector
     */
    protected void move() {
        x = (x < 0) ? xlimit : (x + dx) % xlimit;
        y = (y < 0) ? ylimit : (y + dy) % ylimit;
    }

    /**
     * age is greater than zero
     */
    protected boolean notDeadYet() {
        return lifeSpan > 0;
    }

    /**
     * decrease age by given number of years
     */
    protected void age(int years) {
        lifeSpan -= years;
    }

    /**
     * decrease age by 1
     */
    protected void age() {
        age(1);
    }

    protected void setAge(int years) {
        lifeSpan = years;
    }
}
