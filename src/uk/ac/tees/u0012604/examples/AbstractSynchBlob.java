package uk.ac.tees.u0012604.examples;

import java.awt.*;

public abstract class AbstractSynchBlob extends AbstractBlob {

    public AbstractSynchBlob(Color color, int x, int y, int dx, int dy, int delay) {
        super(color, x, y, dx, dy, delay);
    }

    /**
     * erase the image of the blob
     */
    @Override
    protected synchronized void erase() {
        super.erase();
    }

    /**
     * display the image of the blob at its current coordinates
     */
    @Override
    protected synchronized void display() {
        super.display();
    }
}
