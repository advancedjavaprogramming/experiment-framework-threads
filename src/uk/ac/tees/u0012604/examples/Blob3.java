package uk.ac.tees.u0012604.examples;

import java.awt.*;

public class Blob3 extends AbstractBlob implements Runnable {

    private Thread thread = null;			// the blobs own thread

    public Blob3(Color color, int x, int y, int dx, int dy, int delay) {
        super(color, x, y, dx, dy, delay);
    }

    public void startBlob(Panel canvas, Graphics g, int lifeSpan) {
        if (thread == null || !thread.isAlive()) {
            setAge(lifeSpan);
            setCanvas(canvas);
            setGraphics(g);
            thread = new Thread(this);
            thread.start();
        }
    }

    public void run() {
        // Defs.debug("Blob.run starts");
        display();
        while (notDeadYet()) {
            snooze();					// pause
            erase();					// erase
            age();						// age
            if (notDeadYet()) {
                move();					// move
                display();				// display
            }
        }
        // Defs.debug("Blob.run ends");
    }

}
