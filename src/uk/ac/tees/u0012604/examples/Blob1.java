package uk.ac.tees.u0012604.examples;

import java.awt.*;

public class Blob1 extends AbstractBlob implements Runnable {

    private Thread thread;					// the blobs own thread

    public Blob1(Color color, int x, int y, int dx, int dy, int delay) {
        super(color, x, y, dx, dy, delay);
    }

    @Override
    public void startBlob(Panel canvas, Graphics g, int lifeSpan) {
        setAge(lifeSpan);
        setCanvas(canvas);
        setGraphics(g);
        thread = new Thread(this);
        thread.start();
    }

    @Override
    public void run() {
        // Defs.debug("Blob.run starts");
        display();
        while (notDeadYet()) {
            snooze();					// pause
            erase();					// erase
            age();						// age
            if (notDeadYet()) {
                move();					// move
                display();				// display
            }
        }
        // Defs.debug("Blob.run ends");
    }

}
