package uk.ac.tees.u0012604.examples;

import java.awt.*;

/**
 *
 * @author s
 */
public interface Blobber {

    public void startBlob(Panel canvas, Graphics g, int age);
}
