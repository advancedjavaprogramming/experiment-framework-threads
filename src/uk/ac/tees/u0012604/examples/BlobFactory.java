package uk.ac.tees.u0012604.examples;

/**
 *
 * @author s
 */
public interface BlobFactory {

    public Blobber makeBlob();
}
