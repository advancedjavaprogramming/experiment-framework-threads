package uk.ac.tees.u0012604.examples;

import java.awt.*;

public class Blob2 extends AbstractBlob implements Runnable {

    private Thread thread;					// the blobs own thread
    private boolean isActive = false;		// true when blob is running

    public Blob2(Color color, int x, int y, int dx, int dy, int delay) {
        super(color, x, y, dx, dy, delay);
    }

    @Override
    public void startBlob(Panel canvas, Graphics g, int lifeSpan) {
        if (!isActive) {
            isActive = true;
            setAge(lifeSpan);
            setCanvas(canvas);
            setGraphics(g);
            thread = new Thread(this);
            thread.start();
        }
    }

    @Override
    public void run() {
        // Defs.debug("Blob.run starts");
        display();
        while (notDeadYet()) {
            snooze();					// pause
            erase();					// erase
            age();						// age
            if (notDeadYet()) {
                move();					// move
                display();				// display
            }
        }
        isActive = false;
        // Defs.debug("Blob.run ends");
    }

}
