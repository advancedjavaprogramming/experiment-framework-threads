package uk.ac.tees.u0012604;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import uk.ac.tees.u0012604.examples.BlobFactory;
import uk.ac.tees.u0012604.examples.Blobber;

/**
 * a simple generic wrapper for activating variations of the animated blob,
 * provides a a minimal GUI (one button)
 */
public final class ThreadBallSubPanel extends JFrame {
    // globals

    private Panel canvas;
    private Graphics g;
    private final int FRAME_WIDTH = 500;
    private final int FRAME_HEIGHT = 300;
    private final int DEFAULT_LIFE = 200;

    // global for GUI
    private JTextField lifeSpanField;

    public ThreadBallSubPanel(final String title, final BlobFactory blobFact) {   // NB: title is used to trigger specified activity
        // as well as labelling window

        super();

        Container main = getContentPane();
        main.setLayout(new BorderLayout());
        setTitle(title);
        main.setSize(FRAME_WIDTH, FRAME_HEIGHT); 			// suggest width and height of frame in pixels

        //--- graphics canvas ----------------
        canvas = new Panel();
        main.add(canvas, BorderLayout.CENTER);
        pack();

        //--- cntrlPanel ---------------------
        final JPanel cntrlPanel = new JPanel();
        cntrlPanel.setLayout(new FlowLayout());
        main.add(cntrlPanel, BorderLayout.NORTH);

        cntrlPanel.add(new JLabel("life"));
        lifeSpanField = new JTextField("" + DEFAULT_LIFE, 5);
        cntrlPanel.add(lifeSpanField);

        JButton goBtn = new JButton("Start Ball");
        goBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                Blobber blobber = blobFact.makeBlob();
                //NB: try uncommenting the following line
                // & then explain the results!!
                //Graphics g = canvas.getGraphics();
                blobber.startBlob(canvas, g, Integer.parseInt(lifeSpanField.getText()));
            }
        });
        cntrlPanel.add(goBtn);

        // exit the frame when window is closed
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        // one irritating aspect of Java relates to the necessary ordering of
        // some basic windowing features eg: setSize. For more infm see [*1]
        setSize(FRAME_WIDTH, FRAME_HEIGHT); // suggest width and height of frame in pixels
        setVisible(true);
        g = canvas.getGraphics();
    }

    public Panel getCanvas() {
        return canvas;
    }

}
